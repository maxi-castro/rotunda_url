const FAKE_URL_ORIGN = 'http://example.com';
const stringToCheck = '/6/api/listings/3?sort=desc&limit=10';
const urlFormat = '/:version/api/:collection/:id/';
const urlSection = {};
const urlSearchSection = {};
const regexForNumbers = /^\d+$/;

isValidUrl = (url) => {
    const validURLRegex = /(ftp|http|https):/;
    return validURLRegex.test(url);
}

const urlToTest = (isValidUrl(stringToCheck)) ? new URL(stringToCheck) : new URL(`${FAKE_URL_ORIGN}${stringToCheck}`);

isNumber = (test) => regexForNumbers.test(test);

parseParams = (querystring) => {
    const params = new URLSearchParams(querystring.search);
    
    for (const key of params.keys()) {
        urlSearchSection[key] = (isNumber(params.get(key)) ? Number(params.get(key)) : params.get(key));
    }
};

paserUrl = () => {
    const regexForSplit = /\//;
    const urlVariables = urlFormat.split(regexForSplit);
    const urlPathName = urlToTest.pathname.split(regexForSplit);

    urlVariables.map((item, index) => {
        if (item.includes(':')) {
            const key = item.replace(':', '');
            urlSection[key] = (isNumber(urlPathName[index])) ? Number(urlPathName[index]) : urlPathName[index];
        }
    })

    parseParams(urlToTest);
}

if (stringToCheck) {
    paserUrl();
}

const variables = {...urlSection, ...urlSearchSection}

console.log(variables);
